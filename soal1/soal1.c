#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <time.h>
#include <string.h>
#include <json-c/json.h>

// ----- Function prototypes -----
void make_daemon_process();
char* get_base_dir();
void go_to_base_dir();
void go_to_dir_from_base(char *path);
void download_database();
void gacha_item(int *primo_gems, int harga_item, int *jumlah_gacha);
int get_number_of_files(char *path);
char** list_files(char *path, int number_of_files);
char* pick_random_item(char** file_list, int number_of_files);
char* parse_json_file(int *jumlah_gacha, char* path, char* item, int *sisa_primogems);
// -------------------------------

int main() {
    make_daemon_process();
    // go_to_base_dir();
    
    while (1) {
        time_t now = time(NULL);
        struct tm *timenow = localtime(&now);
        int status;

        go_to_base_dir();
        if (timenow->tm_mon == 2 &&
            timenow->tm_mday == 30 &&
            timenow->tm_hour == 4 &&
            timenow->tm_min == 44) {

            download_database();

            int primo_gems = 79000;
            int harga_item = 160;
            int jumlah_gacha = 0;

            gacha_item(&primo_gems, harga_item, &jumlah_gacha);

            close(STDIN_FILENO);
            close(STDOUT_FILENO);
        }

        if (timenow->tm_mon == 2 &&
            timenow->tm_mday == 30 &&
            timenow->tm_hour == 7 &&
            timenow->tm_min == 44) {

            int banyak_proses = 2;
            pid_t pids[banyak_proses];
            int i = 0;
            
            for (; i < banyak_proses; i++) {
                pids[i] = fork();

                if (pids[i] < 0) {
                    exit(EXIT_FAILURE);
                } else if (i == 0 && pids[i] == 0) {
                    go_to_base_dir();
                    char *args[] = {"zip", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
                    execv("/usr/bin/zip", args);
                } else if (i == 1 && pids[i] == 0) {
                    go_to_base_dir();
                    sleep(10); // menunggu sampai zipping selesai
                    char *args[] = {"rm", "-r", "gacha_gacha", "characters", "weapons", NULL};
                    execv("/usr/bin/rm", args);
                }
            }
            while(wait(&status) > 0);
            // break;
        }
        sleep (10);
    }

    return 0;
}

char* get_base_dir() {
    size_t size = 512;
    char *buff;
    buff = malloc(sizeof(char) * size);
    pid_t pid = getpid();
    char *p;
    char path[64];

    snprintf(path, 64, "/proc/%d/exe", pid);
    ssize_t len = readlink(path, buff, 511);
    if (len != -1) {
        buff[len] = '\0';
        p = strstr(buff, "soal1/");
        strcpy(p, "soal1/\0");
        // printf("path now: %s\n", buff);
        return buff;
    }
    exit(EXIT_FAILURE);
}

void go_to_base_dir() {
    char *base_dir = get_base_dir();
    if (chdir(base_dir) < 0) {
        // printf("go_to_base_dir gagal\n");
        // printf("chdir: %d\n", chdir(get_base_dir()));
        // printf("get base dir: %s\n", get_base_dir(buff));
        exit(EXIT_FAILURE);
    }
    free(base_dir);
    // printf("go_to_base_dir berhasil\n");
    // printf("chdir luar: %d\n", chdir(get_base_dir()));
    // printf("get base dir luar: %s\n", get_base_dir());
}

void go_to_dir_from_base(char *path) {
    char *base_dir = get_base_dir();
    strcat(base_dir, path);
    // printf("base_dir:%s\n", base_dir);
    if (chdir(base_dir) < 0) {
        // printf("go_to_dir_from_base ke-%d gagal\n", tanda);
        exit(EXIT_FAILURE);
    }
    free(base_dir);
    // printf("go_to_dir_from_base ke-%d berhasil\n", tanda);
}

void download_database() {
    int banyak_proses = 5;
    pid_t pids[banyak_proses];
    int status;

    go_to_base_dir();
    int i = 0;
    for (i = 0; i < banyak_proses; i++) {
        pids[i] = fork();

        if (pids[i] == -1) {
            // perror("Fork gagal.\n");
            exit(EXIT_FAILURE);
        } else if (i == 0 && pids[i] == 0) {
            // ini child process 1
            char *args[] = {"mkdir", "gacha_gacha", NULL};
            execv("/usr/bin/mkdir", args);
        } else if (i == 1 && pids[i] == 0) {
            // ini child process 2
            char *args[] = {"curl", "-LOJ", "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", NULL};
            execv("/usr/bin/curl", args);
        } else if (i == 2 && pids[i] == 0) {
            // ini child process 3
            char *args[] = {"curl", "-LOJ", "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", NULL};
            execv("/usr/bin/curl", args);
        } else if (i == 3 && pids[i] == 0) {
            /*
                Ini child process 4
                Untuk meng-unzip database characters
            */
            sleep(10);
            char *args[] = {"unzip", "Anggap_ini_database_characters.zip", NULL};
            execv("/usr/bin/unzip", args);
        } else if (i == 4 && pids[i] == 0) {
            /*
                Ini child process 5
                Untuk meng-unzip database weapons
            */
           sleep(15);
            char *args[] = {"unzip", "Anggap_ini_database_weapon.zip", NULL};
            execv("/usr/bin/unzip", args);
        }
    }

    while (wait(&status) > 0) {
        // menunggu semua child selesai
    }
}

void make_daemon_process() {
    pid_t pid, sid;

    pid = fork();
    switch(pid) {
        case -1:
            exit(EXIT_FAILURE);
        case 0:
            break;
        default:
            exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDERR_FILENO);
}

void gacha_item(int *primo_gems, int harga_item, int *jumlah_gacha) {
    char folder_name[512];
    char file_name[512];
    char **file_list_weapons;
    int number_of_weapon_files;
    char **file_list_characters;
    int number_of_character_files;
    int status;

    number_of_weapon_files = get_number_of_files("weapons");
    file_list_weapons = list_files("weapons", number_of_weapon_files);
    number_of_character_files = get_number_of_files("characters");
    file_list_characters = list_files("characters", number_of_character_files);

    // seed untuk rand()
    srand(time(NULL));

    while ((*primo_gems) > 0) {
        // printf("jumlah gacha:%d\n", (*jumlah_gacha));
        if ((*jumlah_gacha) % 90 == 0) {
            snprintf(folder_name, 511, "gacha_gacha/total_gacha_%d", (*jumlah_gacha));
            
            pid_t mkdir_pid;
            mkdir_pid = fork();
            go_to_base_dir();
            if (mkdir_pid < 0) {
                exit(EXIT_FAILURE);
            } else if (mkdir_pid == 0) {
                // printf("folder_name:%s\n", folder_name);
                char *args[] = {"mkdir", "-p", folder_name, NULL};
                execv("/usr/bin/mkdir", args);
            }
            while(wait(&status) > 0);
        }
        if ((*jumlah_gacha) % 10 == 0) {
            sleep(1);
            time_t now = time(NULL);
            struct tm *timenow = localtime(&now);
            snprintf(file_name, 511, "%02d:%02d:%02d_gacha_%d.txt", timenow->tm_hour, timenow->tm_min, timenow->tm_sec, (*jumlah_gacha));
            // printf("file_name:%s\n", file_name);
        }
        FILE *file_txt;
        char *retrieved_item;
        char *retrieved_item_format;

        if ((*jumlah_gacha) % 2 == 0) {
            retrieved_item = pick_random_item(file_list_weapons, number_of_weapon_files);
            retrieved_item_format = parse_json_file(jumlah_gacha, "weapons", retrieved_item, primo_gems);
        } else if ((*jumlah_gacha) % 2 == 1) {
            retrieved_item = pick_random_item(file_list_characters, number_of_character_files);
            retrieved_item_format = parse_json_file(jumlah_gacha, "characters", retrieved_item, primo_gems);
        }
        go_to_dir_from_base(folder_name);

        file_txt = fopen(file_name, "a");
        fprintf(file_txt, "%s\n", retrieved_item_format);
        
        free(retrieved_item);
        free(retrieved_item_format);
        fclose(file_txt);

        (*primo_gems) -= harga_item;
        (*jumlah_gacha)++;
    }
    free(file_list_characters);
    free(file_list_weapons);
}

int get_number_of_files(char *path) {
    go_to_dir_from_base(path);

    int number_of_files = 0;
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (!d) {
        // printf("opendir number_of_files gagal\n");
        exit(EXIT_FAILURE);
    }

    // hitung berapa banyak jumlah file
    while ((dir = readdir(d)) != NULL) {
        if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")) {
            continue;
        } else {
            number_of_files++;
        }
    }

    closedir(d);
    // printf("number_of_files: %d\n", number_of_files);
    return number_of_files;
}

char** list_files(char *path, int number_of_files) {
    go_to_dir_from_base(path);

    // printf("number_of_files di list_files: %d\n", number_of_files);
    char **file_list = malloc(sizeof(char*) * number_of_files);
    DIR *d;
    struct dirent *dir;
    d = opendir(".");

    int i = 0;
    while((dir = readdir(d)) != NULL) {
        if ( !strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..") )
        {

        }
        else {
            file_list[i] = malloc(strlen(dir->d_name) + 1);
            strncpy(file_list[i], dir->d_name, strlen(dir->d_name));
            file_list[i][strlen(dir->d_name)] = '\0';
            // printf("file_list ke-%d: %s\n", i, file_list[i]);
            i++;
        }
    }

    closedir(d);
    return file_list;
}

char* pick_random_item(char** file_list, int number_of_files) {
    // untuk mendapatkan random number [M, N]:
    // M + rand() / (RAND_MAX / (N - M + 1) + 1)
    unsigned int picked_number = 0 + rand() / (RAND_MAX / (number_of_files - 1 - 0 + 1) + 1);
    // printf("rand: %d, picked_number: %u\n", rand(), picked_number);
    char *picked_item = malloc(sizeof(char) * strlen(file_list[picked_number]) + 1);
    strcpy(picked_item, file_list[picked_number]);
    // printf("picked_item: %s\n", picked_item);
    return picked_item;
}

char* parse_json_file(int *jumlah_gacha, char* path, char* item, int *sisa_primogems) {
    char buff[4096];
    char *hasil = malloc(sizeof(char) * 512);
    FILE *file_json;
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    go_to_dir_from_base(path);
    file_json = fopen(item, "r");
    fread(buff, 4096, 1, file_json);
    fclose(file_json);

    parsed_json = json_tokener_parse(buff);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    snprintf(hasil, 511, "%d_%s_%s_%s_%d", (*jumlah_gacha), path, json_object_get_string(rarity), json_object_get_string(name), (*sisa_primogems));
    return hasil;
}