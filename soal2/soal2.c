#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

void extract()
{
    	pid_t child_id;
    	int status;

    	child_id = fork();

    	// jika fork tidak berhasil
    	if (child_id < 0)
    	{
    	    exit(EXIT_FAILURE);
    	}	

    	// unzip dan simpan pada /home/[USER]/modul2 dengan menggunakan -d
    	if (child_id == 0)
    	{
        	char *argv[] = {"unzip", "-q", "/home/archblaze/drakor.zip", "-d", "/home/archblaze/shift2/drakor", NULL};
        	execv("/usr/bin/unzip", argv);
    	}

    	// pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
    	while (wait(&status) > 0);
}

void removeFD()
{
    	pid_t child_id;
    	int status;

    	child_id = fork();

    	// jika fork tidak berhasil
    	if (child_id < 0)
    	{
		exit(EXIT_FAILURE);
	}

	if (child_id == 0)
    	{
        	char *argv[] = {"find", "/home/archblaze/shift2/drakor/", "-type", "d", "-mindepth", "1", "-exec", "rm", "-rf", "{}", "+", NULL};
        	execv("/usr/bin/find", argv);
    	}

    	// wait agar bisa lanjut ke fungsi selanjutnya
    	while (wait(&status) > 0);
}

void categorize()
{
	pid_t child_id;
	int status;
	DIR *dp;
	struct dirent *ep;
	dp = opendir("/home/archblaze/shift2/drakor");
	
	if (dp != NULL)
	{
		while ((ep = readdir(dp)))
		{
		 	char *folderName;
                	char fileName[100];
                	char folderPath[100];
                       char filePath[100];
                	char rename[100];
                	char renamePath[100];
                	int i = 1;
                	
			strcpy(fileName, ep->d_name);
                	folderName = strtok(ep->d_name, ";");
			
			while(folderName != NULL)
			{
				if (i == 1) 
				{
					strcpy(rename, folderName);
                        		strcat(rename, ".png");
				}
				
				if (i == 3)
				{
					
					if (strstr(folderName, "_") != NULL)
					{
					 	folderName = strtok(folderName, "_");
					 	memset(folderPath, 0, strlen(folderPath));
                            			strcat(folderPath, "/home/archblaze/shift2/drakor/");
                            			strcat(folderPath, folderName);
                            			
                            			child_id = fork();
                            			
                            			if (child_id < 0)
					    	{
					    		// Jika gagal membuat proses baru, program akan berhenti
					       	exit(EXIT_FAILURE);
					   	}
					   	
					   	if (child_id == 0)
					    	{
					       	char *argv[] = {"mkdir", "-p", folderPath, NULL};
					       	execv("/bin/mkdir", argv);
					    	}
					    	
					    	while(wait(&status) > 0);
					}
					else if (strstr(folderName, ".png") != NULL)
					{
		                    		folderName = strtok(folderName, ".");
				            	memset(folderPath, 0, strlen(folderPath));
				            	strcat(folderPath, "/home/archblaze/shift2/drakor/");
				            	strcat(folderPath, folderName);

		                    		child_id = fork();

		                    		if (child_id < 0)
					    	{
					    		// Jika gagal membuat proses baru, program akan berhenti
					       	exit(EXIT_FAILURE);
					   	}
					   	
		                    		if (child_id == 0)
					    	{
					       	char *argv[] = {"mkdir", "-p", folderPath, NULL};
					       	execv("/bin/mkdir", argv);
					    	}
					    	
		                        	while(wait(&status) > 0);
		                	}

					 
					//file akan dipindahkan ke folder genre
					memset(filePath, 0, strlen(filePath));
                        		strcat(filePath, "/home/archblaze/shift2/drakor/");
                        		strcat(filePath, fileName);
                        		
                        		memset(renamePath, 0, strlen(renamePath));
                        		strcat(folderPath, "/");
                        		strcat(renamePath, folderPath);
                        		strcat(folderPath, fileName);
                        		strcat(renamePath, rename);
					
					child_id = fork();
					
					if (child_id < 0)
					{
					    	// Jika gagal membuat proses baru, program akan berhenti
					       exit(EXIT_FAILURE);
					}
					   	
					if (child_id == 0)
					{
					       char *argv[] = {"cp", filePath, folderPath, NULL};
					       execv("/bin/cp", argv); 
					}
					
					else
					{
						while(wait(&status) > 0);
						child_id = fork();
						
						if (child_id < 0)
						{
						    	// Jika gagal membuat proses baru, program akan berhenti
						       exit(EXIT_FAILURE);
						}
						
						if (child_id == 0)
						{
						       char *argv[] = {"mv", folderPath, renamePath, NULL};
						       execv("/bin/mv", argv); 
						}
						while (wait(&status) > 0);
					}
					break;
				}
				
				folderName = strtok(NULL, ";");
                    		i++;
			}
		}
	}
	if (dp != NULL)
	{
		while ((ep = readdir(dp)))
		{
			if (strstr(ep->d_name, "_") != NULL)
			{
				char *folderName;
                		char fileName[100];
                		char folderPath[100];
                       	char filePath[100];
                		char rename[100];
                		char renamePath[100];
                		int i = 1;
                		
                		strcpy(fileName, ep->d_name);
                    		folderName = strtok(ep->d_name, "_");
                    		
                    		while(folderName != NULL)
                    		{
                    		 	if (i == 2)
                    		 	{
                    		 		break;
                    		 	}
                    		 	 
                    		 	folderName = strtok(NULL, "_");
                        		i++;
                    		}
                    		i = 1;
                    		folderName = strtok(folderName, ";");
                    		
                    		while(folderName != NULL)
                    		{
                    			if (i == 1)
                    			{
                    				strcpy(rename, folderName);
                            			strcat(rename, ".png");
                    			}
                    			
                    			if (i == 3)
                    			{
                    				if (strstr(folderName, ".png") != NULL)
                    				{
                    					folderName = strtok(folderName, ".");
                    					
                    					memset(folderPath, 0, strlen(folderPath));
                                			strcat(folderPath, "/home/archblaze/shift2/drakor/");
                                			strcat(folderPath, folderName);
                                			
                                			child_id = fork();
                                			
                                			if(child_id < 0)
							{
								exit(EXIT_FAILURE);
							}
						    	if (child_id == 0)
						    	{
								char *argv[] = {"mkdir", "-p", folderPath, NULL};
								execv("/bin/mkdir", argv);
						    	}
						    	while (wait(&status) > 0);

                    				}
                    			}
                    			//file akan dipindahkan ke folder genre
                    			
                    			memset(filePath, 0, strlen(filePath));
		                    	strcat(filePath, "/home/archblaze/shift2/drakor/");
		                    	strcat(filePath, fileName);

		                    	memset(renamePath, 0, strlen(renamePath));
		                    	strcat(folderPath, "/");
		                    	strcat(renamePath, folderPath);
		                    	strcat(folderPath, fileName);
		                    	strcat(renamePath, rename);
		                    	
		                    	child_id = fork();
					
					if(child_id < 0)
					{
						exit(EXIT_FAILURE);
					}
					if (child_id == 0)
					{
						char *argv[] = {"cp", filePath, folderPath, NULL};
						execv("/bin/cp", argv);
					}
					else
					{
						while(wait(&status) > 0);
						child_id = fork();
						
						if (child_id < 0)
						{
						       exit(EXIT_FAILURE);
						}
						
						if (child_id == 0)
						{
						       char *argv[] = {"mv", folderPath, renamePath, NULL};
						       execv("/bin/mv", argv); 
						}
						while (wait(&status) > 0);
					}
					break;
                    		}
                    		folderName = strtok(NULL, ";");
                    		i++;
			}
		}
	}
	if (dp != NULL)
	{
		while (ep = readdir(dp))
		{
			if (ep->d_type == 4) continue;
			char *tok;
		        char fileName[100];
		        char tahun[100];
		        char genre[100];
		        int i = 1;

                	tok = strtok(ep->d_name, ";");
                	while(tok != NULL)
                	{
                		if (i == 1)
                		{
                       		strcpy(fileName, tok);
                       	}
                    		if (i == 2)
                    		{
                       		strcpy(tahun, tok);
                       	}
				if (i == 3)
				{
                        		if (strstr(tok, "_") != NULL)
                        		{
                            			int c = 1;
                            			tok = strtok(tok, "_");
                            			strcpy(genre, tok);
                        		}
                        		else if (strstr(tok, ".png") != NULL)
                        		{
                            			tok = strtok(tok, ".");
                            			strcpy(genre, tok);
                        		} 
                    		}
			tok = strtok(NULL, ";");
                    	i++;

                	}
                	FILE *outputFile;
		        char directoryGenre[100];
		        strcpy(directoryGenre, "/home/archblaze/shift2/drakor");
		        strcat(directoryGenre, "/");
		        strcat(directoryGenre, genre);
		        strcat(directoryGenre, "/data.txt");

		        outputFile = fopen(directoryGenre, "a");

		        fprintf(outputFile, "%s\n", fileName);
		        fprintf(outputFile, "%s\n", tahun);

		        fclose(outputFile);

		}
	}
	if (dp != NULL)
	{
		while (ep = readdir(dp))
		{
			if (strstr(ep->d_name, "_") != NULL)
			{
				if (ep->d_type == 4) continue;
				char *tok;
				char fileName[100];
				char tahun[100];
				char genre[100];
				int i = 1;

		        	tok = strtok(ep->d_name, "_");
		        	while(tok != NULL)
		        	{
		            		if (i == 2)
		            		{
		               		break;
		               	}
					tok = strtok(NULL, "_");
		            		i++;

		        	}
		        	
		        	i = 1;
                    		tok = strtok(tok, ";");
                    		
                    		while(tok != NULL)
		        	{
		        		if (i == 1)
		        		{
		               		strcpy(fileName, tok);
		               	}
		            		if (i == 2)
		            		{
		               		strcpy(tahun, tok);
		               	}
					if (i == 3)
					{
		                		if (strstr(tok, ".png") != NULL)
		                		{
		                    			tok = strtok(tok, ".");
		                    			strcpy(genre, tok);
		                		} 
		            		}
					tok = strtok(NULL, ";");
		            		i++;

		        	}

		        	FILE *outputFile;
				char directoryGenre[100];
				strcpy(directoryGenre, "/home/archblaze/shift2/drakor");
				strcat(directoryGenre, "/");
				strcat(directoryGenre, genre);
				strcat(directoryGenre, "/data.txt");

				outputFile = fopen(directoryGenre, "a");

				fprintf(outputFile, "%s\n", fileName);
				fprintf(outputFile, "%s\n", tahun);

				fclose(outputFile);

			}
		}
	}
}

int main()
{
	pid_t child_id;
    	int status;
    	child_id = fork();
    	//jika fork tidak berhasil
	if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }
    	if (child_id == 0)
    	{
        	char *argv[] = {"mkdir", "-p", "/home/archblaze/shift2/drakor", NULL};
        	execv("/bin/mkdir", argv);
    	}
    	while (wait(&status) > 0);
    	extract();
    	removeFD();
    	categorize();
}
