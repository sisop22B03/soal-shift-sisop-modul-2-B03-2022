# Laporan Resmi

Ini adalah laporan resmi Modul 2 untuk mata kuliah Sistem Operasi tahun 2022.

**Kelas**: B<br>
**Kelompok**: B03<br>
**Modul**: 2<br>

## Anggota

- Muhamad Ridho Pratama (5025201186)
- Naily Khairiya (5025201244)
- Beryl (5025201029)

## Soal

Soal dapat dilihat di
[sini](https://docs.google.com/document/d/1qWo-VT3bYx8t9Bnpbq_h8Mpn9FpBM3t4T6SWX3vn9RI/edit?usp)
(harus dengan email ITS).

## Dokumentasi

Berikut adalah dokumentasi, cara pengerjaan, dan kendala selama pengerjaan.

### Screenshot

Jika gambar tidak muncul, buka *link* Google Drive pada [README](./README.md)

#### **Soal 1**

- Function prototypes<br>
<img src="https://drive.google.com/uc?id=1eVYOxNFNw5Y2ryx5mqCboitZ-00KQAxp" alt="main function" width="70%"/>
- Function `main()`:<br>
<img src="https://drive.google.com/uc?id=1Ps2OrVXxawToHGBASD8vPchiLiOEtE9_" alt="main function" width="70%"/>
<img src="https://drive.google.com/uc?id=1-8958-Hat9N0WyJRSv5gJUT2EiJCkvQz" alt="main function" width="70%"/>

#### **Soal 2**

<img src="https://drive.google.com/uc?id=1IW-rkV5c-rqi6Gk-es7Xu4Qb9SbQm_j6" alt="main function" width="70%"/>

#### **Soal 3**
- `main()` function : <br>
<img src ="https://drive.google.com/uc?id=1NmtE0djH879p4oPniFB2U7TFg23IlHoe" alt="main function" width="70%">

### Cara Pengerjaan

#### **Soal 1**

1. Buat beberapa prototipe function. Di sini kami membuat:
    - `make_daemon_process()` untuk membuat process menjadi daemon.
    - `get_base_dir()` untuk mendapatkan path ke directory executable (folder soal1)
    - `go_to_base_dir()` untuk `chdir` ke directory soal
    - `go_to_base_dir_from_base()` untuk pergi ke suatu direktori yang terdapat di base direktori
    - `download_database()` untuk mengunduh file zip soal, dan membuat folder sesuai soal
    - `gacha_item()` sebagai function utama untuk meng-gacha
    - `get_number_of_files()` untuk mendapatkan berapa banyak isi file di suatu folder
    - `list_files()` untuk memasukkan nama semua file di suatu folder ke dalam array
    - `pick_random_item()` untuk mengambil item random dari array list file
    - `parse_json_file()` untuk mem-parse file json dan mengambil value dari properties yang dibutuhkan

2. Function `main()`:
    ```c
    int main() {
        make_daemon_process();
        // go_to_base_dir();
        
        while (1) {
            time_t now = time(NULL);
            struct tm *timenow = localtime(&now);
            int status;

            go_to_base_dir();
            if (timenow->tm_mon == 2 &&
                timenow->tm_mday == 30 &&
                timenow->tm_hour == 4 &&
                timenow->tm_min == 44) {

                download_database();

                int primo_gems = 79000;
                int harga_item = 160;
                int jumlah_gacha = 0;

                gacha_item(&primo_gems, harga_item, &jumlah_gacha);

                close(STDIN_FILENO);
                close(STDOUT_FILENO);
            }

            if (timenow->tm_mon == 2 &&
                timenow->tm_mday == 30 &&
                timenow->tm_hour == 7 &&
                timenow->tm_min == 44) {

                int banyak_proses = 2;
                pid_t pids[banyak_proses];
                int i = 0;
                
                for (; i < banyak_proses; i++) {
                    pids[i] = fork();

                    if (pids[i] < 0) {
                        exit(EXIT_FAILURE);
                    } else if (i == 0 && pids[i] == 0) {
                        go_to_base_dir();
                        char *args[] = {"zip", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
                        execv("/usr/bin/zip", args);
                    } else if (i == 1 && pids[i] == 0) {
                        go_to_base_dir();
                        sleep(10); // menunggu sampai zipping selesai
                        char *args[] = {"rm", "-r", "gacha_gacha", "characters", "weapons", NULL};
                        execv("/usr/bin/rm", args);
                    }
                }
                while(wait(&status) > 0);
            }
            sleep (10);
        }

        return 0;
    }
    ```
    1. Pertama, kita jadikan proses ini sebagai proses daemon terlebih dahulu.
    2. Lalu, buat infinite while loop dengan jarak sleep selama 10 detik dengan `sleep(10)`.
    3. Ambil juga waktu sistem saat ini, untuk mengecek apakah saat ini adalah waktu untuk gacha dilakukan. Include header `<time.h>`, dan pada while loop ini, cek apakah waktu sistem saat ini adalah waktu yang sesuai dengan soal, yaitu 30 Maret, pukul 4:44, dan cek juga untuk 3 jam setelahnya.
    4. Jika waktu saat ini adalah 30 Maret pukul 4:44, maka download database dengan function `download_database()`, setelah itu lakukan gacha dengan function `gacha_item()`.
    5. Jika waktu saat ini adalah 30 Maret pukul 7:44, maka buat 2 child process untuk meng-zip file hasil gacha dan menghapus semua direktori/folder.

2. Function `get_base_dir()`, `go_to_base_dir()`, dan `go_to_dir_from_base()`:<br><br>
    **Function `get_base_dir()`:**
    ```c
    char* get_base_dir() {
        size_t size = 512;
        char *buff;
        buff = malloc(sizeof(char) * size);
        pid_t pid = getpid();
        char *p;
        char path[64];

        snprintf(path, 64, "/proc/%d/exe", pid);
        ssize_t len = readlink(path, buff, 511);
        if (len != -1) {
            buff[len] = '\0';
            p = strstr(buff, "soal1/");
            strcpy(p, "soal1/\0");
            // printf("path now: %s\n", buff);
            return buff;
        }
        exit(EXIT_FAILURE);
    }
    ```
    - Pada function `get_base_dir()`, kita akan mendapatkan absolute path ke lokasi direktori/folder executable, yaitu adalah lokasi folder di mana file soal1 berada.
    - Pada Linux, process yang berjalan akan berada di direktori `/proc/`, dengan `<pid>` process tersebut sebagai subfoldernya, dan file `exe` di dalam folder itu, sebagai symlink ke lokasi executable process itu.
    - Jadi, di sini kita akan mendapatkan absolute path ke executable process soal1, dengan cara membaca symlink dari `/proc/<pid process soal1>/exe`.
    - Baca symlink dari file `exe` dengan menggunakan function `readlink()`, taruh pathnya ke dalam suatu buffer/variabel string.
    - Path yang kita dapatkan adalah path ke executable, sedangkan kita membutuhkan path ke direktori di mana executable tersebut berada. Jadi, kita perlu menghapus nama executable di akhir string dari path yang kita dapat.
    - Cari substring tersebut dengan `strstr()`, dan ganti substring itu dengan `strcpy()`.
    - Function ini akan me-return string yang berisi absolute path ke directory executable.<br><br>

    **Function `go_to_base_dir()`:**
    ```c
    void go_to_base_dir() {
        char *base_dir = get_base_dir();
        if (chdir(base_dir) < 0) {
            exit(EXIT_FAILURE);
        }
        free(base_dir);
    }
    ```
    - Pada function ini, kita akan pindah direktori ke direktori soal.
    - Dilakukan dengan pertama memanggil function `get_base_dir()` untuk mendapatkan absolute path ke direktori, setor return value nya ke suatu string.
    - Pindah direktori dengan `chdir(<path yang dituju>)`, cek juga jika return -1, maka `chdir()` gagal.
    - Jangan lupa `free()` pointer to char yang dibuat tadi, agar tidak memakan memori.<br><br>

    **Function `go_to_dir_from_base()`:**
    ```c
    void go_to_dir_from_base(char *path) {
        char *base_dir = get_base_dir();
        strcat(base_dir, path);
        if (chdir(base_dir) < 0) {
            exit(EXIT_FAILURE);
        }
        free(base_dir);
    }
    ```
    - Dengan function ini, process bisa berpindah ke direktori yang ada di dalam base direktori soal1. Process ini menerima argumen pointer to char, yaitu string yang berupa direktori yang dituju.
    - Dilakukan dengan pertama memanggil function `get_base_dir()` untuk mendapatkan absolute path ke direktori, setor return value nya ke suatu string.
    - Lalu, tambahkan/concat path dari argumen ke akhir string yang dibuat dengan `strcat()`.
    - `chdir()` ke direktori dengan argumen dari string yang telah dibuat.
    - Jangan lupa `free()` pointer to char yang dibuat tadi, agar tidak memakan memori.

3. Function `get_number_of_files()`:
    ```c
    int get_number_of_files(char *path) {
        go_to_dir_from_base(path);

        int number_of_files = 0;
        DIR *d;
        struct dirent *dir;
        d = opendir(".");
        if (!d) {
            exit(EXIT_FAILURE);
        }

        // hitung berapa banyak jumlah file
        while ((dir = readdir(d)) != NULL) {
            if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")) {
                continue;
            } else {
                number_of_files++;
            }
        }

        closedir(d);
        return number_of_files;
    }
    ```
    Pada function ini, kita akan mendapatkan banyaknya jumlah file dari suatu direktori, kecuali `.` dan `..` yang merupakan direktori saat ini dan direktori parent.<br>
    - Pertama, kita pindah ke direktori yang akan di-scan.
    - Lalu, gunakan tools header `<dirent.h>`, dan buat pointer ke direktori, dan `struct dirent` untuk alat membaca nama file.
    - Loop sampai semua file terbaca. Pada setiap iterasi, jika nama file bukan `.` atau `..`, maka increment variabel `number_of_files`.
    - Tutup pointer ke direktori dengan `closedir()`.
    - Kembalikan nilai `number_of_files()`.

4. Function `list_files()`:
    ```c
    char** list_files(char *path, int number_of_files) {
        go_to_dir_from_base(path);

        char **file_list = malloc(sizeof(char*) * number_of_files);
        DIR *d;
        struct dirent *dir;
        d = opendir(".");

        int i = 0;
        while((dir = readdir(d)) != NULL) {
            if ( !strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..") )
            {

            }
            else {
                file_list[i] = malloc(strlen(dir->d_name) + 1);
                strncpy(file_list[i], dir->d_name, strlen(dir->d_name));
                file_list[i][strlen(dir->d_name)] = '\0';
                i++;
            }
        }

        closedir(d);
        return file_list;
    }
    ```
    Pada function ini, kita akan menyetor nama file yang ada di direktrori `weapons` atau `characters` ke dalam array.
    - Pertama, pindah ke direktori yang ingin di-scan nama file-nya.
    - Siapkan array of string, `malloc()` agar nilai di dalam array tetap berada di memori, dan bisa di-return saat function selesai.
    - Buat while loop untuk membaca semua file yang ada di direktori.
    - Jika nama file bukan `.` atau `..`, masukkan nama file ke dalam array.
    - Tutup direktori dengan `closedir()` dan return array.

5. Function `pick_random_item()`:
    ```c
    char* pick_random_item(char** file_list, int number_of_files) {
        // untuk mendapatkan random number [M, N]:
        // M + rand() / (RAND_MAX / (N - M + 1) + 1)
        unsigned int picked_number = 0 + rand() / (RAND_MAX / (number_of_files - 1 - 0 + 1) + 1);
        char *picked_item = malloc(sizeof(char) * strlen(file_list[picked_number]) + 1);
        strcpy(picked_item, file_list[picked_number]);
        return picked_item;
    }
    ```
    Pada function ini, kita akan mencari angka acak dari `0` sampai sebanyak `number_of_files - 1`, yaitu banyaknya file di direktori `weapons` atau `characters` dikurang `1`.
    - Kita akan mendapatkan angka acak dengan menggunakan function `rand()`
    - Formula untuk mendapatkan angka acak agar tidak terlalu bias, dengan interval `[M, N]`, adalah: `M + rand() / (RAND_MAX / (N - M + 1) + 1)`. Setor hasil dari angka yang didapat ke variabel `picked_number`.
    - Ambil nama file dari array indeks ke-`picked_number`, dan setor namanya ke string `picked_item`.
    - Return string `picked_item`.

6. Function `parse_json_file()`:
    ```c
    char* parse_json_file(int *jumlah_gacha, char* path, char* item, int *sisa_primogems) {
        char buff[4096];
        char *hasil = malloc(sizeof(char) * 512);
        FILE *file_json;
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        go_to_dir_from_base(path);
        file_json = fopen(item, "r");
        fread(buff, 4096, 1, file_json);
        fclose(file_json);

        parsed_json = json_tokener_parse(buff);
        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        snprintf(hasil, 511, "%d_%s_%s_%s_%d", (*jumlah_gacha), path, json_object_get_string(rarity), json_object_get_string(name), (*sisa_primogems));
        return hasil;
    }
    ```
    Pada function ini, kita akan membaca isi dari file json yang telah dipilih secara acak. Properti yang akan dibaca, yaitu rarity dan name.

7. Function `gacha_item()`:
    ```c
    void gacha_item(int *primo_gems, int harga_item, int *jumlah_gacha) {
        char folder_name[512];
        char file_name[512];
        char **file_list_weapons;
        int number_of_weapon_files;
        char **file_list_characters;
        int number_of_character_files;
        int status;

        number_of_weapon_files = get_number_of_files("weapons");
        file_list_weapons = list_files("weapons", number_of_weapon_files);
        number_of_character_files = get_number_of_files("characters");
        file_list_characters = list_files("characters", number_of_character_files);

        // seed untuk rand()
        srand(time(NULL));

        while ((*primo_gems) > 0) {
            if ((*jumlah_gacha) % 90 == 0) {
                snprintf(folder_name, 511, "gacha_gacha/total_gacha_%d", (*jumlah_gacha));
                
                pid_t mkdir_pid;
                mkdir_pid = fork();
                go_to_base_dir();
                if (mkdir_pid < 0) {
                    exit(EXIT_FAILURE);
                } else if (mkdir_pid == 0) {
                    char *args[] = {"mkdir", "-p", folder_name, NULL};
                    execv("/usr/bin/mkdir", args);
                }
                while(wait(&status) > 0);
            }
            if ((*jumlah_gacha) % 10 == 0) {
                sleep(1);
                time_t now = time(NULL);
                struct tm *timenow = localtime(&now);
                snprintf(file_name, 511, "%02d:%02d:%02d_gacha_%d.txt", timenow->tm_hour, timenow->tm_min, timenow->tm_sec, (*jumlah_gacha));
            }
            FILE *file_txt;
            char *retrieved_item;
            char *retrieved_item_format;

            if ((*jumlah_gacha) % 2 == 0) {
                retrieved_item = pick_random_item(file_list_weapons, number_of_weapon_files);
                retrieved_item_format = parse_json_file(jumlah_gacha, "weapons", retrieved_item, primo_gems);
            } else if ((*jumlah_gacha) % 2 == 1) {
                retrieved_item = pick_random_item(file_list_characters, number_of_character_files);
                retrieved_item_format = parse_json_file(jumlah_gacha, "characters", retrieved_item, primo_gems);
            }
            go_to_dir_from_base(folder_name);

            file_txt = fopen(file_name, "a");
            fprintf(file_txt, "%s\n", retrieved_item_format);
            
            free(retrieved_item);
            free(retrieved_item_format);
            fclose(file_txt);

            (*primo_gems) -= harga_item;
            (*jumlah_gacha)++;
        }
        free(file_list_characters);
        free(file_list_weapons);
    }
    ```
    Function ini adalah function utama untuk meng-gacha item.
    - Pertama, inisialisasi dulu variabel untuk nama file, nama folder, array untuk nama-nama file json, dan banyak jumlah file json.
    - Lalu, dapatkan banyak jumlah file untuk masing-masing direktori `weapons` dan `characters` dengan function `get_number_of_files()`. Assign hasilnya ke suatu variabel.
    - Lalu, dapatkan juga list nama-nama file json yang ada di direktori `weapons` dan juga `characters`. Setor hasilnya ke array.
    - Seed waktu saat ini agar `rand()` pada function `pick_random_item()` bisa bekerja.
    - Lalu, buat while loop selama jumlah `primo_gems` masih lebih besar dari `0`.
    - Dalam loop ini, kita akan membuat beberapa kondisi.
        - Kondisi pertama adalah saat `jumlah_gacha % 90 == 0`. Di sini, kita akan membuat folder baru dengan format nama folder `total_gacha_{jumlah-gacha}`. Kita membuat folder dengan cara membuat child proses, lalu melakukan `mkdir` dengan `execv`.
        - Kondisi kedua adalah saat `jumlah_gacha % 10 == 0`. Di sini, kita akan membuat sebuah file `txt` dengan format nama file `{Hh:Mm:Ss}_gacha_{jumlah-gacha}.txt`
        - Kondisi ketiga adalah saat `jumlah_gacha` ganjil maupun genap. Di sini, kita akan meng-gacha item dengan function `pick_random_item()`, dan mendapatkan format namanya dengan function `parse_json_file()`. Format untuk setiap gacha adalah `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`. String ini akan di-append ke file `txt` yang telah dibuat tadi.
    - Terakhir, jangan lupa `free()` semua pointer agar tidak memakan memori.

#### **Soal 2**

```c
int main()
{
    pid_t child_id;
    int status;
    child_id = fork();
    if(child_id < 0)
    {
            exit(EXIT_FAILURE);
    }
        if (child_id == 0)
        {
            char *argv[] = {"mkdir", "-p", "/home/archblaze/shift2/drakor", NULL};
            execv("/bin/mkdir", argv);
        }
        while (wait(&status) > 0);
        extract();
        removeFD();
        categorize();
}
```
Pada fungsi main ini, step pertama adalah melakukan fork untuk membuat direktori drakor pada `/home/archblaze/shift2/drakor` 
Setelah itu, akan memanggil fungsi `extract()`, `removeFD()`, dan `categorize()`.

```c
void extract()
{
        pid_t child_id;
        int status;

        child_id = fork();

        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }	

        if (child_id == 0)
        {
            char *argv[] = {"unzip", "-q", "/home/archblaze/drakor.zip", "-d", "/home/archblaze/shift2/drakor", NULL};
            execv("/usr/bin/unzip", argv);
        }

        while (wait(&status) > 0);
}
```
Pada fungsi ini, dengan menggunakan fork akan dilakukan extraksi pada file `drakor.zip` kedalam direktori `home/archblaze/shift2/drakor`.

```c
void removeFD()
{
        pid_t child_id;
        int status;

        child_id = fork();

        if (child_id < 0)
        {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0)
        {
            char *argv[] = {"find", "/home/archblaze/shift2/drakor/", "-type", "d", "-mindepth", "1", "-exec", "rm", "-rf", "{}", "+", NULL};
            execv("/usr/bin/find", argv);
        }

        while (wait(&status) > 0);
}
```
Pada fungsi ini, dengan fork akan dilakukan penghapusan folder yang tidak dibutuhkan dalam direktori `home/archblaze/shift2/drakor`, menyisakan file file dengan format png. 

```c
void categorize()
{
    pid_t child_id;
    int status;
    DIR *dp;
    struct dirent *ep;
    dp = opendir("/home/archblaze/shift2/drakor");
    
    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
             char *folderName;
                    char fileName[100];
                    char folderPath[100];
                       char filePath[100];
                    char rename[100];
                    char renamePath[100];
                    int i = 1;
                    
            strcpy(fileName, ep->d_name);
                    folderName = strtok(ep->d_name, ";");
            
            while(folderName != NULL)
            {
                if (i == 1) 
                {
                    strcpy(rename, folderName);
                                strcat(rename, ".png");
                }
                
                if (i == 3)
                {
                    
                    if (strstr(folderName, "_") != NULL)
                    {
                         folderName = strtok(folderName, "_");
                         memset(folderPath, 0, strlen(folderPath));
                                        strcat(folderPath, "/home/archblaze/shift2/drakor/");
                                        strcat(folderPath, folderName);
                                        
                                        child_id = fork();
                                        
                                        if (child_id < 0)
                            {
                                // Jika gagal membuat proses baru, program akan berhenti
                               exit(EXIT_FAILURE);
                           }
                           
                           if (child_id == 0)
                            {
                               char *argv[] = {"mkdir", "-p", folderPath, NULL};
                               execv("/bin/mkdir", argv);
                            }
                            
                            while(wait(&status) > 0);
                    }
                    else if (strstr(folderName, ".png") != NULL)
                    {
                                    folderName = strtok(folderName, ".");
                                memset(folderPath, 0, strlen(folderPath));
                                strcat(folderPath, "/home/archblaze/shift2/drakor/");
                                strcat(folderPath, folderName);

                                    child_id = fork();

                                    if (child_id < 0)
                            {
                                // Jika gagal membuat proses baru, program akan berhenti
                               exit(EXIT_FAILURE);
                           }
                           
                                    if (child_id == 0)
                            {
                               char *argv[] = {"mkdir", "-p", folderPath, NULL};
                               execv("/bin/mkdir", argv);
                            }
                            
                                    while(wait(&status) > 0);
                            }

                     
                    //file akan dipindahkan ke folder genre
                    memset(filePath, 0, strlen(filePath));
                                strcat(filePath, "/home/archblaze/shift2/drakor/");
                                strcat(filePath, fileName);
                                
                                memset(renamePath, 0, strlen(renamePath));
                                strcat(folderPath, "/");
                                strcat(renamePath, folderPath);
                                strcat(folderPath, fileName);
                                strcat(renamePath, rename);
                    
                    child_id = fork();
                    
                    if (child_id < 0)
                    {
                            // Jika gagal membuat proses baru, program akan berhenti
                           exit(EXIT_FAILURE);
                    }
                           
                    if (child_id == 0)
                    {
                           char *argv[] = {"cp", filePath, folderPath, NULL};
                           execv("/bin/cp", argv); 
                    }
                    
                    else
                    {
                        while(wait(&status) > 0);
                        child_id = fork();
                        
                        if (child_id < 0)
                        {
                                // Jika gagal membuat proses baru, program akan berhenti
                               exit(EXIT_FAILURE);
                        }
                        
                        if (child_id == 0)
                        {
                               char *argv[] = {"mv", folderPath, renamePath, NULL};
                               execv("/bin/mv", argv); 
                        }
                        while (wait(&status) > 0);
                    }
                    break;
                }
                
                folderName = strtok(NULL, ";");
                            i++;
            }
        }
    }
    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strstr(ep->d_name, "_") != NULL)
            {
                char *folderName;
                        char fileName[100];
                        char folderPath[100];
                           char filePath[100];
                        char rename[100];
                        char renamePath[100];
                        int i = 1;
                        
                        strcpy(fileName, ep->d_name);
                            folderName = strtok(ep->d_name, "_");
                            
                            while(folderName != NULL)
                            {
                                 if (i == 2)
                                 {
                                     break;
                                 }
                                  
                                 folderName = strtok(NULL, "_");
                                i++;
                            }
                            i = 1;
                            folderName = strtok(folderName, ";");
                            
                            while(folderName != NULL)
                            {
                                if (i == 1)
                                {
                                    strcpy(rename, folderName);
                                        strcat(rename, ".png");
                                }
                                
                                if (i == 3)
                                {
                                    if (strstr(folderName, ".png") != NULL)
                                    {
                                        folderName = strtok(folderName, ".");
                                        
                                        memset(folderPath, 0, strlen(folderPath));
                                            strcat(folderPath, "/home/archblaze/shift2/drakor/");
                                            strcat(folderPath, folderName);
                                            
                                            child_id = fork();
                                            
                                            if(child_id < 0)
                            {
                                exit(EXIT_FAILURE);
                            }
                                if (child_id == 0)
                                {
                                char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                execv("/bin/mkdir", argv);
                                }
                                while (wait(&status) > 0);

                                    }
                                }
                                //file akan dipindahkan ke folder genre
                                
                                memset(filePath, 0, strlen(filePath));
                                strcat(filePath, "/home/archblaze/shift2/drakor/");
                                strcat(filePath, fileName);

                                memset(renamePath, 0, strlen(renamePath));
                                strcat(folderPath, "/");
                                strcat(renamePath, folderPath);
                                strcat(folderPath, fileName);
                                strcat(renamePath, rename);
                                
                                child_id = fork();
                    
                    if(child_id < 0)
                    {
                        exit(EXIT_FAILURE);
                    }
                    if (child_id == 0)
                    {
                        char *argv[] = {"cp", filePath, folderPath, NULL};
                        execv("/bin/cp", argv);
                    }
                    else
                    {
                        while(wait(&status) > 0);
                        child_id = fork();
                        
                        if (child_id < 0)
                        {
                               exit(EXIT_FAILURE);
                        }
                        
                        if (child_id == 0)
                        {
                               char *argv[] = {"mv", folderPath, renamePath, NULL};
                               execv("/bin/mv", argv); 
                        }
                        while (wait(&status) > 0);
                    }
                    break;
                            }
                            folderName = strtok(NULL, ";");
                            i++;
            }
        }
    }
    if (dp != NULL)
    {
        while (ep = readdir(dp))
        {
            if (ep->d_type == 4) continue;
            char *tok;
                char fileName[100];
                char tahun[100];
                char genre[100];
                int i = 1;

                    tok = strtok(ep->d_name, ";");
                    while(tok != NULL)
                    {
                        if (i == 1)
                        {
                            strcpy(fileName, tok);
                        }
                            if (i == 2)
                            {
                               strcpy(tahun, tok);
                            }
                if (i == 3)
                {
                                if (strstr(tok, "_") != NULL)
                                {
                                        int c = 1;
                                        tok = strtok(tok, "_");
                                        strcpy(genre, tok);
                                }
                                else if (strstr(tok, ".png") != NULL)
                                {
                                        tok = strtok(tok, ".");
                                        strcpy(genre, tok);
                                } 
                            }
            tok = strtok(NULL, ";");
                        i++;

                    }
                    FILE *outputFile;
                char directoryGenre[100];
                strcpy(directoryGenre, "/home/archblaze/shift2/drakor");
                strcat(directoryGenre, "/");
                strcat(directoryGenre, genre);
                strcat(directoryGenre, "/data.txt");

                outputFile = fopen(directoryGenre, "a");

                fprintf(outputFile, "%s\n", fileName);
                fprintf(outputFile, "%s\n", tahun);

                fclose(outputFile);

        }
    }
    if (dp != NULL)
    {
        while (ep = readdir(dp))
        {
            if (strstr(ep->d_name, "_") != NULL)
            {
                if (ep->d_type == 4) continue;
                char *tok;
                char fileName[100];
                char tahun[100];
                char genre[100];
                int i = 1;

                    tok = strtok(ep->d_name, "_");
                    while(tok != NULL)
                    {
                            if (i == 2)
                            {
                               break;
                           }
                    tok = strtok(NULL, "_");
                            i++;

                    }
                    
                    i = 1;
                            tok = strtok(tok, ";");
                            
                            while(tok != NULL)
                    {
                        if (i == 1)
                        {
                               strcpy(fileName, tok);
                           }
                            if (i == 2)
                            {
                               strcpy(tahun, tok);
                           }
                    if (i == 3)
                    {
                                if (strstr(tok, ".png") != NULL)
                                {
                                        tok = strtok(tok, ".");
                                        strcpy(genre, tok);
                                } 
                            }
                    tok = strtok(NULL, ";");
                            i++;

                    }

                    FILE *outputFile;
                char directoryGenre[100];
                strcpy(directoryGenre, "/home/archblaze/shift2/drakor");
                strcat(directoryGenre, "/");
                strcat(directoryGenre, genre);
                strcat(directoryGenre, "/data.txt");

                outputFile = fopen(directoryGenre, "a");

                fprintf(outputFile, "%s\n", fileName);
                fprintf(outputFile, "%s\n", tahun);

                fclose(outputFile);

            }
        }
    }
}
```
Pada fungsi ini, dengan fork berdasarkan beberapa kasus akan melakukan kategorisasi file gambar berdasarkan kategori yang berada pada nama file itu.
Setelah itu, akan dibuat file `data.txt` pada direktori `home/archblaze/shift2/drakor` dengan berisikan data nama, tahun, dan kategori dari file-file tersebut.



### **Soal no 3**

1. `main()` sebagai driver di dalam program. Pada `main()` akan dipanggil fungsi-fungsi lain.

```c
    //pembuatan 2  folder
	for(int i=0; i<2; i++)
	{
		createFolder(i);
	}

	download();
	extract();

	//pemindahan darat dan air
	for(int i=0; i<2; i++)
	{
		move(i);
	}

	//menghapus file tanpa keterangan
	//menghapus bird pada directory /home/[USER]/modul2/darat
	removeFD(animalFolder, "f", "*.*");
	removeFD(folder[0], "f", "*bird*");

	//Membuat file list.txt
	createFile("/home/naily/modul2/air/list.txt");

	//membuat list nama semua hewan yang ada
	listFiles(folder[1]);

```

- looping `for` digunakan untuk membuat 2 folder dan memindahkan file ke 2 folder
- menghapus file tanpa keterangan pada `home/[USER]/modul2/animal` dan mengahapus burung pada folder darat menggunakan fungsi yang sama yaitu `removeFD()`
- pada list folder yang di list adalah `folder[1]` atau folder `home/[USER]/modul2/air`

2. Fungsi `CreateFolder()` digunakan untuk membuat folder

```c
void createFolder(int idx)
{
   	pid_t child_id = fork();
   	int status;

	if (child_id < 0)
    	{
    		// Jika gagal membuat proses baru, program akan berhenti
       		exit(EXIT_FAILURE);
   	}

	//membuat folder satu persatu berdasarkan idx, ada darat dan air
    	if (child_id == 0)
    	{
       		char *argv[] = {"mkdir", "-p", folder[idx], NULL};
       		execv("/bin/mkdir", argv);
    	}

    	//pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
    	while(wait(&status) > 0);

	//proses pembuatan folder lain dengan jeda 3s
	sleep(3);
}

```

- parameter fungsi `CreateFolder()` bertipe `int` yang merupakan index. Index digunakan untuk pemanggilan 2 kali untuk membuat folder darat dan juga air. Untuk index 0 adalah folder untuk membuat folder darat serta index 1 untuk membuat folder air. 


3. Fungsi `download()` digunakan untuk mendownload file `animal.zip` dari drive
```c
void download()
{
	pid_t child_id;
    	int status;

    	child_id = fork();

	//jika fork tidak berhasil
        if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

	//gunakan wget untuk mendownload dan langsung rename dengan -O menjadi animal.zip
    	if(child_id == 0)
	{
        	char *argv[] = {"wget", "-q", url, "-O", fileName, NULL};
        	execv("/usr/bin/wget", argv);
    	}

    	//pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
   	 while(wait(&status) > 0);
}
```
- Fungsi ini tidak diminta pada soal
- Fungsi ini dibuat supaya memudahkan memasukkan file `animal.zip` pada `home/[USER]/modul2`
- Download dengan menggunakan `wget`
- `-O` digunakan untuk merename file hasil download


4. Fungsi `extract()` digunakan untuk mengekstrak file `animal.zip` 
```c
void extract()
{
	pid_t child_id;
	int status;

	child_id = fork();

	//jika fork tidak berhasil
	if(child_id < 0)
	{
		exit(EXIT_FAILURE);
	}

	//unzip dan simpan pada /home/[USER]/modul2 dengan menggunakan -d
	if(child_id==0)
	{
        	char *argv[] = {"unzip", "-q", fileName, "-d", dir, NULL};
        	execv("/usr/bin/unzip", argv);
	}

	//pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
	while(wait(&status) > 0);
}
```

- Untuk mengekstrak file `animal.zip` menggunakan `unzip`
- `-d` digunakan untuk menyimpan hasil ekstrak pada suatu directory


5. Fungsi `move()` digunakan untuk memindahkan file dari suatu directory ke directory yang lain
```c
void move(int idx)
{
	pid_t child_id;
	int status;

	child_id = fork();

	//jika fork tidak berhasil
	if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

	//pindahkan ke folder darat terlebih dahulu
	//jeda 3s untuk memindahkan ke folder air
	if(child_id == 0)
	{
		char *argv[] = {"find", animalFolder, "-type", "f", "-iname", fname[idx], "-exec", "mv", "{}", folder[idx], ";", NULL};
		execv("/usr/bin/find", argv);
	}
		
	//jeda 3 detik ke folder selanjutnya
	sleep(3);
	
	//wait supaya bisa lanjut ke proses selanjutnya
	while(wait(&status) > 0 );
}
```

- Memindahkan file dengan bantuan `find`
- Tipenya `-f` karena berupa file
- `-iname` digunakan untuk mendapatkan file dengan keterangan tertentu
- Untuk hewan darat maka gunakan `"*darat*"` sedangkan untuk air gunakan `"*air*"`
- Kemudian menggunakan `-exec mv` untuk memindahkannya
- Dan pindahkan ke folder yang dituju

6. Fungsi `removeFD()` untuk menghapus
```c
void removeFD(char dirr[], char type[], char keyword[])
{
	pid_t child_id;
	int status;

	child_id = fork();

	//jika fork tidak berhasil
        if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

	if(child_id == 0)
	{
		char *argv[] = {"find", dirr, "-type", type, "-name", keyword, "-exec", "rm", "-r", "{}", "+", NULL};
		execv("/usr/bin/find", argv);
	}
	
	//wait agar bisa lanjut ke fungsi selanjutnya
	while(wait(&status) > 0);
}
```

- Untuk menghapus menggunakan bantuan `find`
- Untuk tipe karena yang akan dihapus adalah file maka `f`
- Untuk yang akan dihapus apabila terdapat keterangan khusus semisal `bird` maka pada bagian keyword dimasukkan `*bird*`
- `"rm -r {} +"` digunakan untuk menghapus secara rekursif

7. Untuk me list file yang ada pada folder air make perlu membuat file `list.txt` terlebih dahulu dengan fungsi `createFile()`
 ```c
 void createFile(char fileName[])
{
	pid_t child_id;
	int status;

	child_id  = fork();

	//jika fork tidak berhasil
        if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

        if(child_id == 0)
        {
                char *argv[] = {"touch", fileName, NULL};
		execv("/usr/bin/touch", argv);
        }
    	while(wait(&status) > 0 );
}
 ```
- Untuk membuat file kosong baru dengan `touch`

8. Fungsi `listFile()` digunakan untuk list file yang ada pada folder air dan diletakkan pada file `list.txt`
```c
void listFiles(char *basePath)
{
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{
			.
			.
			.

			//memasukkan format ke text file (list.txt)
			if(strcmp(dp -> d_name, "list.txt") != 0)
				printText(format);

			//membuat path baru dari base path
			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp -> d_name);

			listFiles(path);
		}
	}

	closedir(dir);
}
```
- Dengan bantuan library `dirent.h` maka dapat didapatkan list file
- Untuk mendapatkan format yang sesuai dengan soal maka perlu mendapatkan UID dengan memanggil fungsi `getUID()` 
- Serta perlu mendapatkan permission dengan memanggil fungsi `permission()`
- Dan untuk memasukkan list file ke `list.txt` perlu panggil fungsi `printText()`

9. Fungsi `getUID()` untuk mendapatkan UID
```c
const char* getUID(char path[])
{
	struct stat info;
	int r;

	r = stat(path, &info);
    	if( r==-1 )
    	{
        	fprintf(stderr,"File error\n");
        	exit(EXIT_FAILURE);
    	}

    	struct passwd *pw = getpwuid(info.st_uid);

   	if (pw != 0)
	{
		return (pw->pw_name);
	}
	else
	{
		return "ERROR";
	}
}
```
- Fungsi `getpwuid()` akan mencari entri database pengguna dengan uid yang cocok
- Fungsi `getpwuid()` mengembalikan pointer ke struct passwd dengan struktur seperti yang didefinisikan dalam `<pwd.h>` dengan entri yang cocok jika ditemukan.

10. Fungsi `permissiion()` digunakan untuk mendapatkan permission dari suatu file
```c
const char* permission(char path[])
{
	struct stat fs;
	char filePermission[100] = {};
	char *sPermission = filePermission;
	int r;

	r = stat(path, &fs);
	if(r == -1)
	{
		fprintf(stderr, "File error\n");
		exit(EXIT_FAILURE);
	}

	//OWNER PERMISSIONS
	if(fs.st_mode & S_IRUSR)
		strcat(filePermission, "r");
	else
		strcat(filePermission, "-");

	if(fs.st_mode & S_IWUSR)
                strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IXUSR )
        	strcat(filePermission, "x");
        else
                strcat(filePermission, "-");
	.
	.
	.
	return sPermission;
}
```

- Tiga item dikelompokkan ke dalam rwx untuk izin baca, tulis, dan eksekusi
- Set pertama adalah untuk pemilik, pembuat file; 
- Set kedua adalah untuk grup pengguna; 
- Set terakhir adalah untuk pengguna yang tidak berada dalam grup tertentu
- Bit izin dapat dipastikan menggunakan bidang st_mode dari struct yang dikembalikan oleh fungsi stat
- Bit individu dapat diekstraksi menggunakan konstanta `S_IRUSR` (User Read), `S_IWUSR` (User Write), `S_IRGRP` (Group Read) dan lainnya


11. Fungsi `printText()` digunakan untuk menampung list file yang ada di folder air pada file `list.txt` 
```c
void printText(char text[])
{
	char *filename = "/home/naily/modul2/air/list.txt";

	//membuka file untuk dapat memodifikasi
	//"a" supaya bisa append text
	FILE *fp = fopen(filename, "a");

	if(fp == NULL)
	{
		printf("ERROR SAAT MEMBUKA FILE\n");
		exit(EXIT_FAILURE);
	}

	//masukkan isi ke text file
	fprintf(fp, "%s\n", text);

	//menutup file
	fclose(fp);
}
```

- Di awal perlu membuka file `list.txt` 
- Gunakan `"a"` supaya dapat append daftar file ke `list.txt`
- Apabila filenya tidak ada akan memunculkan pesan Error
- Print list file ke `list.txt`
- Tutup file supaya aman





### Kendala Saat Pengerjaan

#### **Soal 1**

- Tidak tahu bagaimana cara mengambil time system.
- Saat mengambil time system, ternyata setiap kali kita mau mengambil time, harus membuat variabel `time_t` dan `struct tm` baru lagi. Tidak bisa menggunakan yang lama.
- Tidak tahu bagaimana cara mengambil absolute path ke executable

#### **Soal 2**

- Kesulitan pada bagian pemindahan dan pemilahan file berdasarkan kategorinya masing-masing ke dalam folder dengan nama kategori tersebut.

#### **Soal 3**
- Diawal masih ada sedikit kebingungan dalam penggunaan `fork` dan `wait`

## Referensi
- [Google](https://www.google.com)
- [StackOverflow](https://stackoverflow.com)
- [Unix & Linux Stack Exchange](https://unix.stackexchange.com/)
- [GitHub Modul 2 Sistem Operasi](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2)
