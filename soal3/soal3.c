#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define EXIT_FAILURE -1
#define EXIT_SUCCESS 0

char url[] = "https://drive.google.com/uc?id=1LauKPhyLsUTNKJXo46Dj127OIMFwOVYY&export=download";
char fileName[] = "/home/naily/modul2/animal.zip";
char dir[] = "/home/naily/modul2";
char animalFolder[] = "/home/naily/modul2/animal";
char *folder[] = {"/home/naily/modul2/darat", "/home/naily/modul2/air"};
char *fname[] = {"*darat*", "*air*"};


//Gunakan fungsi ini supaya bisa gunakan exit failure
extern void exit();


// Membuat folder dengan menggunakan fork dan exec
// Fork berguna untuk membuat proses baru atau child proses
// Setelah berhasil dengan exec untuk membuat directory
// jeda 3 detik dengan sleep
void createFolder(int idx)
{
	// Membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat”
   	// lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”
   	pid_t child_id = fork();
   	int status;

	if (child_id < 0)
    	{
    		// Jika gagal membuat proses baru, program akan berhenti
       		exit(EXIT_FAILURE);
   	}

	//membuat folder satu persatu berdasarkan idx, ada darat dan air
    	if (child_id == 0)
    	{
       		char *argv[] = {"mkdir", "-p", folder[idx], NULL};
       		execv("/bin/mkdir", argv);
    	}

    	//pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
    	while(wait(&status) > 0);

	//proses pembuatan folder lain dengan jeda 3s
	sleep(3);
}


//Fungsi download() untuk mendownload file dari drive
//menyimpan hasil download pada directory yang diinginkan
//merename file hasil download
void download()
{
	pid_t child_id;
    	int status;

    	child_id = fork();

	//jika fork tidak berhasil
        if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

	//gunakan wget untuk mendownload dan langsung rename dengan -O menjadi animal.zip
    	if(child_id == 0)
	{
        	char *argv[] = {"wget", "-q", url, "-O", fileName, NULL};
        	execv("/usr/bin/wget", argv);
    	}

    	//pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
   	 while(wait(&status) > 0);
}


//fungsi extract untuk mengunzip atau mengextract animal.zip
//simpan pada directory yang diinginkan
void extract()
{
	pid_t child_id;
	int status;

	child_id = fork();

	//jika fork tidak berhasil
	if(child_id < 0)
	{
		exit(EXIT_FAILURE);
	}

	//unzip dan simpan pada /home/[USER]/modul2 dengan menggunakan -d
	if(child_id==0)
	{
        	char *argv[] = {"unzip", "-q", fileName, "-d", dir, NULL};
        	execv("/usr/bin/unzip", argv);
	}

	//pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
	while(wait(&status) > 0);
}


//hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya.
//Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat”
//dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”
void move(int idx)
{
	pid_t child_id;
	int status;

	child_id = fork();

	//jika fork tidak berhasil
	if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

	//pindahkan ke folder darat terlebih dahulu
	//jeda 3s untuk memindahkan ke folder air
	if(child_id == 0)
	{
		char *argv[] = {"find", animalFolder, "-type", "f", "-iname", fname[idx], "-exec", "mv", "{}", folder[idx], ";", NULL};
		execv("/usr/bin/find", argv);
	}
		
	//jeda 3 detik ke folder selanjutnya
	sleep(3);
	
	//wait supaya bisa lanjut ke proses selanjutnya
	while(wait(&status) > 0 );
}


//hapus hewan tanpa keterangan
//hapus file zip supaya lebih rapi
//hapus file burung
void removeFD(char dirr[], char type[], char keyword[])
{
	pid_t child_id;
	int status;

	child_id = fork();

	//jika fork tidak berhasil
        if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

	if(child_id == 0)
	{
		char *argv[] = {"find", dirr, "-type", type, "-name", keyword, "-exec", "rm", "-r", "{}", "+", NULL};
		execv("/usr/bin/find", argv);
	}
	
	//wait agar bisa lanjut ke fungsi selanjutnya
	while(wait(&status) > 0);
}


//fungsi yang akan mengembalikan UID
//dimana UID adalah user dari file tersebut
const char* getUID(char path[])
{
	struct stat info;
	int r;

	r = stat(path, &info);
    	if( r==-1 )
    	{
        	fprintf(stderr,"File error\n");
        	exit(EXIT_FAILURE);
    	}

    	struct passwd *pw = getpwuid(info.st_uid);

   	if (pw != 0)
	{
		return (pw->pw_name);
	}
	else
	{
		return "ERROR";
	}
}


//fungsi permission adalah untuk mencari permission dari file yang dimaksud
//format rwxrwxrwx
//r akses untuk read atau membaca file
//w akses untuk write atau memodifikasi file
//x akses untuk execute atau melihat file
//dimana 3 huruf pertama menunjukkan permission untuk user,
//3 huruf selanjutnya permission untuk group
// dan 3 huruf terakhir adalah other permission
//apabila tidak ada permission untuk r, w, ataupun x akan digantikan dengan -
const char* permission(char path[])
{
	struct stat fs;
	char filePermission[100] = {};
	char *sPermission = filePermission;
	int r;

	r = stat(path, &fs);
	if(r == -1)
	{
		fprintf(stderr, "File error\n");
		exit(EXIT_FAILURE);
	}

	//OWNER PERMISSIONS
	if(fs.st_mode & S_IRUSR)
		strcat(filePermission, "r");
	else
		strcat(filePermission, "-");

	if(fs.st_mode & S_IWUSR)
                strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IXUSR )
        	strcat(filePermission, "x");
        else
                strcat(filePermission, "-");


	//GROUP PERMISSIONS
	if( fs.st_mode & S_IRGRP )
        	strcat(filePermission, "r");
        else
                strcat(filePermission, "-");

    	if( fs.st_mode & S_IWGRP )
        	strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

    	if( fs.st_mode & S_IXGRP )
        	strcat(filePermission, "x");
        else
                strcat(filePermission, "-");


	//OTHER PERMISSIONS
	if( fs.st_mode & S_IROTH )
		strcat(filePermission, "r");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IWOTH )
		strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IXOTH )
		strcat(filePermission, "x");
        else
                strcat(filePermission, "-");

	return sPermission;
}


//fungsi createFile untuk membuat file kosong
void createFile(char fileName[])
{
	pid_t child_id;
	int status;

	child_id  = fork();

	//jika fork tidak berhasil
        if(child_id < 0)
        {
                exit(EXIT_FAILURE);
        }

        if(child_id == 0)
        {
                char *argv[] = {"touch", fileName, NULL};
		execv("/usr/bin/touch", argv);
        }
    	while(wait(&status) > 0 );
}


//fungsi untuk print pada Text file yaitu listfile
void printText(char text[])
{
	char *filename = "/home/naily/modul2/air/list.txt";

	//membuka file untuk dapat memodifikasi
	//"a" supaya bisa append text
	FILE *fp = fopen(filename, "a");

	if(fp == NULL)
	{
		printf("ERROR SAAT MEMBUKA FILE\n");
		exit(EXIT_FAILURE);
	}

	//masukkan isi ke text file
	fprintf(fp, "%s\n", text);

	//menutup file
	fclose(fp);
}


//fungsi untuk list isi directory air
//akan memanggil fungsi lain yang dibutuhkan
//seperti mendapatkan UID dan permission File
void listFiles(char *basePath)
{
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{

			char fileUID[250];
			char filePermission[20];
			char tmpDir[250];
			char format[250];

			strcpy(tmpDir, folder[1]);
			strcat(tmpDir, "/");
			strcat(tmpDir, dp -> d_name);

			//untuk mendapatkan UID
			strcpy(fileUID, getUID(tmpDir));

			//untuk mendapatkan file permission
			strcpy(filePermission, permission(tmpDir));

			//menggabungkan UID, file permission dan nama file
			strcpy(format, fileUID);
			strcat(format, "_");
			strcat(format, filePermission);
			strcat(format, "_");
			strcat(format, dp -> d_name);

			//memasukkan format ke text file (list.txt)
			if(strcmp(dp -> d_name, "list.txt") != 0)
				printText(format);

			//membuat path baru dari base path
			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp -> d_name);

			listFiles(path);
		}
	}

	closedir(dir);
}



//fungsi main sebagai driver
int main()
{
	//pembuatan 2  folder
	for(int i=0; i<2; i++)
	{
		createFolder(i);
	}

	download();
	extract();

	//pemindahan darat dan air
	for(int i=0; i<2; i++)
	{
		move(i);
	}


	//menghapus file tanpa keterangan
	//menghapus bird pada directory /home/[USER]/modul2/darat
	removeFD(animalFolder, "f", "*.*");
	removeFD(folder[0], "f", "*bird*");

	//Membuat file list.txt
	//di folder “/home/[USER]/modul2/air
	createFile("/home/naily/modul2/air/list.txt");

	//membuat list nama semua hewan yang ada
	//di directory “/home/[USER]/modul2/air” ke “list.txt”
	//dengan format UID_[UID file permission]_Nama File.[jpg/png] 
	listFiles(folder[1]);

	return 0;
}
